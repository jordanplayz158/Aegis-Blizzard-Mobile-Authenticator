package com.beemdevelopment.aegis.importers;

import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Xml;

import com.beemdevelopment.aegis.encoding.Base32;
import com.beemdevelopment.aegis.encoding.EncodingException;
import com.beemdevelopment.aegis.encoding.Hex;
import com.beemdevelopment.aegis.otp.OtpInfo;
import com.beemdevelopment.aegis.otp.OtpInfoException;
import com.beemdevelopment.aegis.otp.TotpInfo;
import com.beemdevelopment.aegis.util.PreferenceParser;
import com.beemdevelopment.aegis.vault.VaultEntry;
import com.topjohnwu.superuser.io.SuFile;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class BattleNetImporter extends DatabaseImporter {
    private static final String _pkgName = "com.blizzard.bma";
    private static final String _subPath = "shared_prefs/com.blizzard.bma.AUTH_STORE.xml";

    public BattleNetImporter(Context context) {
        super(context);
    }

    @Override
    protected SuFile getAppPath() throws DatabaseImporterException, PackageManager.NameNotFoundException {
        return getAppPath(_pkgName, _subPath);
    }

    @Override
    protected State read(InputStream stream, boolean isInternal) throws DatabaseImporterException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(stream, null);
            parser.nextTag();

            List<String> entries = new ArrayList<>();
            for (PreferenceParser.XmlEntry entry : PreferenceParser.parse(parser)) {
                if (entry.Name.equals("com.blizzard.bma.AUTH_STORE.HASH")) {
                    entries.add(entry.Value);
                    break;
                }
            }
            return new BattleNetImporter.State(entries);
        } catch (XmlPullParserException | IOException e) {
            throw new DatabaseImporterException(e);
        }
    }

    public static class State extends DatabaseImporter.State {
        private List<String> _entries;

        public State(List<String> entries) {
            super(false);
            _entries = entries;
        }

        @Override
        public Result convert() {
            Result result = new Result();

            for (String str : _entries) {
                try {
                    VaultEntry entry = convertEntry(str);
                    result.addEntry(entry);
                } catch (DatabaseImporterEntryException e) {
                    result.addError(e);
                }
            }

            return result;
        }

        private static VaultEntry convertEntry(String hash) throws DatabaseImporterEntryException {
            try {
                final int[] mask = {57,142,39,252,80,39,106,101,
                        96,101,176,229,37,244,192,108,
                        4,198,16,117,40,107,142,122,
                        237,165,157,169,129,59,93,214,
                        200,13,47,179,128,104,119,63,
                        165,155,164,124,23,202,108,100,
                        121,1,92,29,91,139,143,107,
                        154};

                byte[] hashBytesBase16Decoded = Hex.decode(hash);

                StringBuilder unmaskingStringBuilder = new StringBuilder();

                for (int i = 0 ; i < hashBytesBase16Decoded.length; i++) {
                    int hashByte = hashBytesBase16Decoded[i];
                    int maskByte = mask[i];

                    // This is my only complaint with this solution, using the python script as a reference
                    // I see no reason why this is needed but after many hours of investigation
                    // some bytes were negative and offset by EXACTLY 256 and this fixes it
                    if(hashByte < 0) {
                        hashByte += 256;
                    }

                    int character = (hashByte ^ maskByte);

                    unmaskingStringBuilder.append((char) character);
                }

                String unmasking = unmaskingStringBuilder.toString();

                byte[] secretHexBytes = unmasking.substring(0, 40).getBytes(StandardCharsets.UTF_8);
                String secretHex = new String(secretHexBytes);

                byte[] secret = Hex.decode(secretHex);

                OtpInfo info = new TotpInfo(secret, OtpInfo.DEFAULT_ALGORITHM, 8, TotpInfo.DEFAULT_PERIOD);

                return new VaultEntry(info, "BMA", "Battle.net");
            } catch (OtpInfoException | EncodingException e) {
                throw new DatabaseImporterEntryException(e, hash);
            }
        }
    }
}
